## 一
    npm init -y 
    package.json中main设置为main.js || script : "start":"electron ."
    npm i electron -d
## electron 具有的功能
1. 窗口创建：main process（入口文件main.js）中可以创建，render process中可以创建
2. 打开外部网络连接
3. 向url发送请求
4. 更改窗口菜单
5. 设置快捷键
6. 设置右下角应用图标
7. 设置右下角右键弹出菜单
8. 打开文件（文件功能）
