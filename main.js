const electron = require("electron");
const app = electron.app;
const BrowserWindow = electron.BrowserWindow;
//const {app, BrowserWindow} = require('electron')
const path = require('path')
const url = require('url')


let win1,win2;

function createWindow () {

  win1 = new BrowserWindow({width: 800, height: 600,      webPreferences: {
    nodeIntegration: true,   
    enableRemoteModule: true, 
    contextIsolation: false, 
}})

win2 = new BrowserWindow({width: 800, height: 600,      webPreferences: {
    nodeIntegration: true,   
    enableRemoteModule: true, 
    contextIsolation: false, 
}})

  win1.loadURL(url.format({
    pathname: path.join(__dirname, 'index.html'),
    protocol: 'file:',
    slashes: true
  }));

  win2.loadURL(url.format({
    pathname: path.join(__dirname, 'index.html'),
    protocol: 'file:',
    slashes: true
  }));


  win1.on('closed', () => {
    win1 = null
  });
  win2.on('closed', () => {
    win2 = null
  });
}

app.on('ready', createWindow);